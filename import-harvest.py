"""
HOW TO DELETE

for i in $(curl -u "USER:PWD" \
            -H 'Accept: application/json' \
            "https://your_org.harvestapp.com/people/1403728/entries?from=20160901&to=20161128" 2> /dev/null \
        | jq .[].day_entry.id); \
    do curl -X DELETE -u "USER:PWD" \
            -H 'Accept: application/json' "https://your_org.harvestapp.com/daily/delete/$i"; \
done
"""
import json
import argparse
import requests

def main(**args):
    rows = args['rows']
    for r in rows:
        item = dict(zip(args['head'].split(','), r.split(',')))
        payload = {
            "notes": item.get("Notes"),
            "hours": float(item.get("Hours")),
            "project_id": "7078826",
            "task_id": "3559435",
            "spent_at": item.get("Date")
        }
        print(payload)
        url = "https://your_org.harvestapp.com/daily/add"
        resp = requests.post(url, auth=(USERNAME, PASSWORD),
                             data=json.dumps(payload),
                             headers={'Accept': 'application/json',
                                      'Content-Type': 'application/json'})
        print(resp.status_code)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Import CSV data in Harvest app')
    parser.add_argument('csv_data', help='Raw CSV data')
    head, rows = parser.parse_known_args()
    args = {
        'head': head.csv_data,
        'rows': rows
    }
    main(**args)
