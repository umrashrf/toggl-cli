#!/usr/bin/env python

import json
import logging
import argparse
import requests

from urllib2 import quote
from datetime import datetime


TOGGL_APIKEY = ""


def get_toggl_entries(**args):
    start_date = args.get('start_date') + quote('T00:00:00-05:00')
    end_date = args.get('end_date') + quote('T23:59:59-05:00')

    url = "https://www.toggl.com/api/v8/time_entries?start_date=%(start_date)s&end_date=%(end_date)s" % dict(
            start_date=start_date, end_date=end_date)
    resp = requests.get(url, auth=(TOGGL_APIKEY, "api_token"))
    logging.info("Fetching time entries from %s" % url)
    time_entries = []
    for line in json.loads(resp.content):
        if "description" not in line:
            line["description"] = ""
        desc = line["description"]
        hours = round(float(line["duration"])/3600, 2)
        time_entries.append((line["start"], desc.strip(), hours))
    time_entries = sorted(time_entries, key=lambda x: x[1])
    return time_entries

def main(**args):
    # header
    print '"Date","Client","Project","Task","Notes","Hours","First name","Last name"'

    total_hours = 0.0
    toggl_entries = get_toggl_entries(**args)
    for entry in toggl_entries:
        if entry[2] < 0:
            continue
        total_hours += entry[2]
        print '"%s","Your Org Inc","Cool Project 1","Development","%s","%s","Umair","Ashraf"' % (
                entry[0].split('T')[0], entry[1].replace(',', ' '), entry[2])

    # FIXME: after logging into Redmine, set tags for toggl items
    # https://github.com/toggl/toggl_api_docs/blob/master/chapters/time_entries.md#update-a-time-entry

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Push Toggl entries to Redmine.')
    parser.add_argument('start_date', help='Start date of toggl entries.')
    parser.add_argument('end_date', nargs='?', help='End date of toggl entries.',
                        default=datetime.utcnow().strftime('%Y-%m-%d'))
    args = vars(parser.parse_args())
    main(**args)
