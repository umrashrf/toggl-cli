# toggl-cli is a collection of scripts to import/export data between time trackers

## HOW TO USE?

1. Export toggl entries
```$ python export-toggle.py 2016-09-01```

2. Count toggl entries
```$ python export-toggle.py 2016-09-01 | wc -l```

3. Import toggl entries into Harvest app
```$ python export-toggle.py 2016-09-01 | xargs python import-harvest.py```
